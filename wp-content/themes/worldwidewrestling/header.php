<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package worldwidewrestling
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style>
	.w3-sidebar a {font-family: "Roboto", sans-serif}
	body,h1,h2,h3,h4,h5,h6,.w3-wide {font-family: "Montserrat", sans-serif;}

	.custom_menu{
		padding: 8px 16px;
		float:left;
		width:auto;
		border:none;
		display:block;
		outline:0;
	}

	</style>
	<?php wp_head(); ?>
</head>

<body class="w3-content" style="max-width:1200px">
	
	<!-- Menu on small screens -->
	<nav id="site-navigation" class="w3-bar w3-top w3-hide-large w3-black w3-xlarge">
		<div class="w3-bar-item w3-padding-24 w3-wide">
			<h3>World Wide Wrestling</h3>
		</div>
		<a href="javascript:void(0)" class="w3-bar-item w3-button w3-padding-24 w3-right" onclick="w3_open()"><i class="fa fa-bars"></i></a>
	</nav><!-- #site-navigation -->

	<!-- Sidebar navigation -->
	<nav class="w3-sidebar w3-bar-block w3-white w3-collapse w3-top" style="z-index:3;width:250px" id="mySidebar">
		<div class="w3-container w3-display-container w3-padding-16">
			<i onclick="w3_close()" class="fa fa-remove w3-hide-large w3-button w3-display-topright"></i>
			<h3 class="w3-wide"><b><img src="https://shop.weare3dub.com/images/logo.png" style="width:50%;height:auto"/></b></h3>
		</div>
		<div class="w3-padding-64 w3-large w3-text-grey" style="font-weight:bold">

			<?php
			$menuParameters = array(
				'container'       => false,
				'echo'            => false,
				'items_wrap'      => '%3$s',
				'depth'           => 0,
				'item_class'	  => 'w3-button'
			);
				echo wp_nav_menu( $menuParameters )
			?>
		</div>
	</nav>
	<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

	<div class="w3-main" style="margin-left:250px">
			
		<div class="w3-hide-large" style="margin-top:120px"></div>

			<div id="content">
