<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package worldwidewrestling
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header" style="background-image: url('http://staging.weare3dub.com/wp-content/uploads/2019/06/1-1.jpg');width:100%;height:200px">
		<?php the_title( '<div class="w3-display-topleft w3-text-white" style="padding:24px 48px"><h1>', '</h1>' ); ?>
		<p><a href="https://shop.weare3dub.com/" class="w3-button w3-yellow w3-padding-large" style="text-transform:uppercase">Visit our webshop</a></p>
		</div>
	</header><!-- .entry-header -->

	<div class="w3-display-container w3-container w3-center">
		<hr/>
	</div>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'worldwidewrestling' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'worldwidewrestling' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
