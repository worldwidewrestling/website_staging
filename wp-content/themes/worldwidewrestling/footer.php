<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package worldwidewrestling
 */

?>

	</div><!-- #content -->

	<div class="w3-display-container w3-container w3-black w3-center">
    <h3>World Wide Wrestling</h3>
    <p>World Wide Wrestling is the leading e-fed in the world. With innovative storylines, a high production quality and solid 
    talent management, we have set ourselves apart from every other e-fed around the world. If you wish to see more from 3DUB, please check our
    YouTube page or apply for our Facebook group to get updated on the latest 3DUB information.</p>
    <div class="w3-half">
      <a href="https://www.youtube.com/channel/UCulJdDYtT58oKHFiT8JwTrQ"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIoNCPIJf4qAr7m2wLBU2f7LbO4W8MC7A1mHtGGprJnZRwVyEj" style="width:100px;height:auto">
    </div>
    <div class="w3-half">
      <a href="https://www.facebook.com/groups/704816956330667/"><img src="http://www.exclusive.cars/resources/images/social-fb.png" style="width:100px;height:auto">
    </div>
</div>

<script>
// Accordion 
function myAccFunc() {
  var x = document.getElementById("demoAcc");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else {
    x.className = x.className.replace(" w3-show", "");
  }
}

// Click on the "Jeans" link on page load to open the accordion for demo purposes
document.getElementById("myBtn").click();


// Open and close sidebar
function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("myOverlay").style.display = "block";
}
 
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("myOverlay").style.display = "none";
}
</script>


<script>
function openCity(evt, cityName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the link that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();

</script>


</body>
</html>

<?php wp_footer(); ?>

</body>
</html>
